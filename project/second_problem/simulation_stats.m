% Run simulations, then use data to calculate
% data, plot stuff, etc. Change false to true to enable
% a section.
num_simulations = 1000;
probs = [0.6 0.05 0.15 0.2]; % first
%probs = [0.25 0.60 0.10 0.05]; % second

ts = zeros(1, num_simulations);
totalpops = zeros(1, num_simulations);
maxpops = zeros(1, num_simulations);
xs = {};

longest_time = 0;
largest_maxpop = 0;
largest_totpop = 0;

% Do some simulations
for i = 1:num_simulations
    [t totalpop maxpop x_pop] = simulate_population(probs);
    ts = [ts t]; 
    totalpops = [totalpops totalpop];
    maxpops = [maxpops maxpop]; 
    xs{i} = x_pop;
    
    if t > longest_time
        longest_time = t;
        longest_time_index = i;
    end
    
    if maxpop > largest_maxpop
        largest_maxpop = maxpop;
        largest_maxpop_index = i;
    end
    
    if totalpop > largest_totpop
        largest_totpop = totalpop;
        largest_totpop_index = i;
    end
end

% Plot a figure showing three populations:
% Longest time, maximum population, highest total population
if true
    clf % clear figure
    hold on

    % Plot the longest living species
    %figure
    plot(xs{longest_time_index}, 'r')

    % Plot the tribe with the highest maximum population
    %figure
    plot(xs{largest_maxpop_index}, 'g')

    % Plot the tribe with the highest total population
    %figure
    plot(xs{largest_totpop_index}, 'b-.')
    legend('Longest time', 'Maximum population', 'Highest total population')
    ylabel('Population')
    xlabel('Generation')
end
