% Test the discrete sampling function for correct distribution

values =  [  1    2   3    4]; % must be over 0
probs  =  [0.6 0.05 0.15 0.2];

total_values = 100000;

samples = zeros(1, length(values));
for i = 1:total_values
    sample = discrete_rand(values, probs);
    samples(sample) = samples(sample) + 1;
end

estimated_probs = samples/total_values;
estimated_probs
probs