function [ nextpop ] = next_population( currentpop, probs )
%NEXT_POPULATION returns amount of individuals in the next generation
    nextpop = sum(randsample([0 1 2 3], currentpop, true, probs));
end

