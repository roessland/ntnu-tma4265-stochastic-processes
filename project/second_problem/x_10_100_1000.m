num_simulations = 100000;
offspring = [0 1 2 3];
%probs = [0.6 0.05 0.15 0.2]; % first
probs = [0.25 0.60  0.10 0.05]; % second

x10 = zeros(1, num_simulations);
x100 = zeros(1, num_simulations);
x1000 = zeros(1, num_simulations);

times = zeros(1, num_simulations);
totalpops = zeros(1, num_simulations);
maxpops = zeros(1, num_simulations);

parfor i=1:num_simulations
    [t totalpop maxpop x] = simulate_population(probs);
    if t > 10
        x10(i) = x(10 + 1);
    end
    
    if t > 100
        x100(i) = x(100 + 1);
    end
    
    if t > 1000
        x1000(i) = x(1000 + 1);
    end
    
    times(i) = t;
    totalpops(i) = totalpop;
    maxpops(i) = maxpop;
end



if false
    % Calculate variance for the number of offspring produced
    % by a single individual. p246 in Ross.
    s_squared = 0;
    for i=1:4
       s_squared = s_squared + (offspring(i) - 0.95)^2 * probs(i); 
    end

    for n = [10 100 1000]
       std_a(n) = sqrt(s_squared*0.95^(n-1)*(1-0.95^n)/(1-0.95));
    end
    disp('---')
    mean(x10)
    0.95^10
    std(x10)
    std_a(10)
    disp('---')
    mean(x100)
    0.95^100
    std(x100)
    std_a(100)
    disp('---')
    mean(x1000)
    0.95^1000
    std(x1000)
    std_a(1000)
    disp('---')
end

if true
   % Plot histograms for times, totalpops, maxpops
   close all
   hist(times, sqrt(num_simulations));
   xlim([0 50])
   ylabel('Frequency')
   xlabel('Survival time')
   legend('Histogram of survival times')
   figure
   hist(totalpops, 40);
   xlim([0 50])
   ylabel('Frequency')
   xlabel('Total population')
   legend('Histogram of total population')
   figure
   hist(maxpops, sqrt(num_simulations));
   xlim([0 20])
   ylabel('Frequency')
   xlabel('Maximum population')
   legend('Histogram of maximum population')
   
end
