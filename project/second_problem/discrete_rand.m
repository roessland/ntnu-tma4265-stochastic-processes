function [ value ] = discrete_rand( values, probs )
%DISCRETE_RAND returns a sample from a discrete probability distribution
%   values: possible values that can be returned
%   probs:  the corresponding probability for each value
    
    % Create inverse cumulative distribution function
    icdf = cumsum(probs);
    
    % Sample from it
    rand_num = rand(); % (0, 1)
    for i = 1:length(probs)
       if rand_num <= icdf(i)
           value = values(i);
           return;
       end
    end
end

