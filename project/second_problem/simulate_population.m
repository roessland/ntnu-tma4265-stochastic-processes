function [ t totalpop maxpop x] = simulate_population( probs )
% SIMULATE_POPULATION runs simulation, returns data
% t = time until extinction
% totalpop = sum of populations in all generations
% maxpop = maximum population in all generations
% x = array of population sizes for each generation not extinct
    maxpop = 1;
    totalpop = 1;
    x(0 + 1) = 1;
    t = 0;
    while x(t + 1) ~= 0
        t = t + 1;
        x(t + 1) = next_population(x(t), probs);
        totalpop = totalpop + x(t + 1);
        maxpop = max(maxpop, x(t + 1));
    end
end