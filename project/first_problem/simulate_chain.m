% Simulate and plot a chain for 50 time steps
% x: one-indexing, X: zero-indexing
% So that x(i+1) = X(i)

clear all
x(0 + 1) = 0;
p = 0.75; % base probability

t = 0;
% Invariant: x(0 + 1) to x(i + 1) have been created
while t <= 49
   t = t + 1;
   % Gets state X(i) from state X(i-1)
   x(t + 1) = next_state(x(t), p);
end

plot(0:t, x)
set(gca,'XTick',[0:10 12:2:50])
set(gca,'YTick',[0:50])
ylabel('State')
xlabel('Time')
grid on