% This script calculates the probability distribution
% of pi_j, using simulations

clear all

p = 0.75;
initial = 0; % Use 0 to 9
final = 10;
simulations = 4; % Increase for p=0.9

time_sum = 0;
% To run with more CPU cores: matlabpool local 4
parfor i = 1:simulations
    time_sum = time_sum + time_until_state(initial, final, p);
end

mean = time_sum/simulations;
initial
mean