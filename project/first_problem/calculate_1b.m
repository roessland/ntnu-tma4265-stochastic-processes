% Calculate expected time until reaching state 10
% given that we start in state i: mu(i+1)
p = 0.75; 

% Generate probability matrix
M = zeros(11);
M(1,1) = -p;
M(1,2) = p;
for n=2:10
   M(n,n-1) = 1-p^n;
   M(n,n) = -1;
   M(n,n+1) = p^n;
end
M(11,11) = 1;

b = [-1; -1; -1; -1; -1; -1; -1; -1; -1; -1; 0];

% Solve for my
mu = M\b;
mu