% This script estimates pi_j theoretically using a
% recurrence relation, and numerically, by simulating 
% a long chain, and plots the results

% Estimate pi_j theoretically
p = 0.9;
pi = [];
pi(0+1) = 1;
pi(1+1) = p*pi(0+1)/(1-p^2);

for n = 1:1000000
    pi(n+1+1) = (pi(n+1) - pi(n-1+1)*p^n)/(1-p^(n+2));
end

pi = pi/sum(pi); % Normalize

plot(0:15, pi(1:16), 'r')
hold on

% Estimate pi_j using numerical simulation
x(0 + 1) = 0;
n = 100; % simulate up to X(100)
t = 0;

clear pi
pi = zeros(1, 100);
pi(0 + 1) = 1;

% Simulate a chain for t = 100000
for t = 1:100000
    try % Just in case we get over 100
        pi(x(t)+1) = pi(x(t)+1) + 1;
    catch
        pi(x(t)+1) = 1;
    end
    x(t + 1) = next_state(x(t), p);
end

pi = pi/sum(pi); % Normalize
plot(0:15, pi(1:16),'g')

legend('\pi_j from recurrence relation', '\pi_j from simulation')
ylabel('Probability')
xlabel('State')
set(gca,'XTick',[0:15])

 