function [ t ] = time_until_state( initial_state,final_state, p)
%GET_NEXT_STATE returns time the chain took to reach final_state
    x(0 + 1) = initial_state;

    t = 0;
    % Invariant: X(0) to X(t) have been created
    while x(t + 1) ~= final_state
       t = t + 1;
       % Gets state X(i) from state X(i-1)
       x(t + 1) = next_state(x(t), p);
    end
end

