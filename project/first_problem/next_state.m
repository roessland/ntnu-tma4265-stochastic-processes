function [ j ] = next_state( i, p )
%GET_NEXT_STATE returns next state in the Markov chain
    rand_prob = rand(1);
    if i == 0
        if rand_prob < 1 - p
            j = 0;
        else
            j = 1;
        end
    elseif rand_prob < p^(i+1)
        j = i + 1;
    else
        j = i - 1;
    end
end

