% Estimate the probability that the time for the first visit to
% state 15 is greater than or equal to 1000, for i = 0 and i = 10

clear all

base_prob = 0.9; % Run for 0.75 and 0.9
initial = 10; % Run for 0 and 10
final = 15;
simulations = 1000;
n = 1000;

greater_equal_n = 0;
times = [];
% To run with more CPU cores: matlabpool local 4
parfor i = 1:simulations
    time_spent = time_until_state(initial, final, base_prob);
    times = [times time_spent];
    if time_spent >= n
        greater_equal_n = greater_equal_n + 1;
    end
end

probability = greater_equal_n / simulations;
probability 